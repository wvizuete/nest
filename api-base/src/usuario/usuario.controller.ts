import {Controller, Get, Delete, Post, Put} from '@nestjs/common';

@Controller('usuario')
export class UsuarioController {
    // @Get('/obtener-usuario')
    @Get('/lista-usuarios')
    getUsuarios(){
        return {
            mensaje: "lista de usuarios"
        }
    }
// http://localhost:3000/usuario/eliminar/6
// http://localhost:3000/usuario/eliminar/23
    @Delete('/eliminar/:id')
    eliminarUsuario(){
        return {
            mensaje: 'usuario eliminado'
        }
    }

    // http://localhost:3000/usuario/23
    @Get(':id')
    getUsuarioPorId(){
        return {
            mensaje: "por id"
        }
    }

    @Post('crear')
    crearUsuario(){
        return {
            mensaje: 'usuario creado'
        }
    }

    @Put('actualizar')
    actualizarUsuario(){
        return {
            mensaje: 'usuario actualizado'
        }
    }
    
}
